import React from 'react';
import LMLButton from './components/button.js';
import Datum from './components/datum.js';
import './styles.css';

class LMLDatasetEditor extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h1>Hello {this.props.name} from Mi LMLDatasetEditor!</h1>
                <LMLButton></LMLButton>
                <Datum onUpdateDataset={this.props.onUpdateDataset}></Datum>
            </div>
        );
    }
}

export default LMLDatasetEditor;