import React from 'react';

class Datum extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = { dataset: "Dataset 0" };
        
        this.updateDataset = this.updateDataset.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    updateDataset(e) {
        //this.props.onUpdateDataset(this.state.dataset);
        this.props.onUpdateDataset(
            {
                nombre: 'juanda',
                apellidos: 'rodríguez'
            }
        );
    }

    handleChange(event) {
        this.setState({ dataset: event.target.value });
    }

    render() {
        return (
            <div>
                <p> El estado es: {this.state.dataset}</p>
                <div>
                    <input value={this.state.dataset} onChange={this.handleChange}/>
                    <button onClick={this.updateDataset} >actualiza estado</button>
                </div>

            </div>

        )
    }
}

export default Datum;