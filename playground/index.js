import React from 'react';
import { render } from 'react-dom';
import LMLDatasetEditor from '../src';

let handleUpdateDataset = function (dataset) {
    console.log("desde App");
    console.log(dataset);
}

const App = () => (
    <div>
        <LMLDatasetEditor
            name="juanda"
            onUpdateDataset={handleUpdateDataset} />
    </div>

);

render(<App />, document.getElementById("root"));