const path = require('path');

module.exports = {
    entry: path.join(__dirname, "src/index.js"),
    output: {
        filename: 'index.js',
        path: path.join(__dirname, '/dist'),
        library: '',
        libraryTarget: 'commonjs'
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    devServer: {
        port: 3001
    }
};