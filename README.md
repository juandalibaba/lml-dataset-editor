Para ejecutar el playground y desarrollar el componente
independientemente de Scratch:

    # npm start

Se abre servidor en puerto 3001

Para crear una versión distribuible (transpilada a ES5):

    # npm run transpile

Si se quiere ir probando en local no es necesario publicarlo en npm, 
basta con hacer en el directorio raíz de este proyecto:

    # npm link

Y en el proyecto donde se quiera incluir este componente (por ejemplo
en scratch-gui):

    # npm link lml-dataset-editor

